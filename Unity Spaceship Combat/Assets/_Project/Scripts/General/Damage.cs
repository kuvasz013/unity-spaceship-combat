﻿public class Damage
{
    public float amount;
    public DamageType type;

    public Damage(float _amount, DamageType _type)
    {
        amount = _amount;
        type = _type;
    }
}
